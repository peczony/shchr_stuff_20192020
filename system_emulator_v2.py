#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import division
import os
import copy
import codecs
import argparse
import math
import json
import re
import datetime
import logging

# import formulae
from collections import defaultdict, Counter
import pyexcel
import pyaml
import gspread
from gspread.models import Cell
from oauth2client.service_account import ServiceAccountCredentials

scope = [
    "https://spreadsheets.google.com/feeds",
    "https://www.googleapis.com/auth/drive",
]
credentials = ServiceAccountCredentials.from_json_keyfile_name(
    "Shchr-111d5fe2be03.json", scope
)
gc = gspread.authorize(credentials)

try:
    basestring
except:
    basestring = str


def tryint(obj):
    try:
        return int(obj)
    except:
        return


def write_standings(teams, type_=None):
    for team in teams:
        if (
            team["standing"] in {1, "1", 2, "2", 3, "3"}
            or type_ == "С"
            and tryint(team["standing"])
        ):
            team["questions_for_sorting"] = team["questions_total"] + (
                (100 - int(team["standing"])) / 1000.0
            )
        else:
            team["questions_for_sorting"] = team["questions_total"]
    pointslist = sorted(
        [team["questions_for_sorting"] for team in teams], reverse=True
    )
    by_category = {}
    categories = {x["category"] for x in teams}
    for category in categories:
        by_category[category] = sorted(
            [
                team["questions_for_sorting"]
                for team in teams
                if team["category"] == category
            ],
            reverse=True,
        )
    for team in teams:
        pointslist_category = by_category[team["category"]]
        team["standing_for_scoring"] = [
            pointslist.index(team["questions_for_sorting"]) + 1,
            len(pointslist)
            - pointslist[::-1].index(team["questions_for_sorting"]),
        ]
        team["standing_for_scoring_in_category"] = [
            pointslist_category.index(team["questions_for_sorting"]) + 1,
            len(pointslist_category)
            - pointslist_category[::-1].index(team["questions_for_sorting"]),
        ]


def normalize_name(name):
    result = str(name).lower()
    result = result.replace("ё", "е")
    result = re.sub(r"[^a-zа-яa-zё0-9]+", "", result)
    return result


class FileAdapter(logging.LoggerAdapter):
    def process(self, msg, kwargs):
        return (
            "{} [{}] {}".format(
                datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                self.extra["filename"],
                msg,
            ),
            kwargs,
        )


town_to_region = json.load(codecs.open("town_to_region.json", "r", "utf8"))

main_logger = logging.getLogger(__file__)
main_logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
fh = logging.FileHandler(
    "{}/{}.log".format(
        os.path.dirname(os.path.abspath(__file__)),
        datetime.datetime.now().strftime("%Y-%m-%dT%H-%M-%S"),
    ),
    mode="w",
)
fh.setLevel(logging.DEBUG)
main_logger.handlers = [ch, fh]
logger = FileAdapter(main_logger, {"filename": "main"})

type_dict = {"__type": ""}

names = {}

renames = {}
for p in [
    ("единаяимперя", "RU48"),
    ("единаяимперия", "RU48"),
    ("единаяимперiя", "RU48"),
    ("единаяимперія", "RU48"),
]:
    renames[p] = "Единая Имперiя"

renames_usage = Counter()

code_to_region = {}


class GetField(object):
    def __init__(self, en_name):
        self.en_name = en_name

    def __call__(self, row, dct):
        try:
            dct[self.en_name] = (
                process_string(row[1])
                if isinstance(row[1], basestring)
                else row[1]
            )
            if not dct[self.en_name]:
                logger.error("Поле [{}] не заполнено".format(row[0]))
        except IndexError:
            logger.error("Не удаётся извлечь поле [{}]".format(row[0]))


class GetQuantity(object):
    def __init__(self, en_name):
        self.en_name_ = en_name

    def __call__(self, row, dct):
        self.en_name = "{}{}".format(type_dict["__type"], self.en_name_)
        try:
            dct[self.en_name] = row[1]
        except IndexError:
            logger.error("Не удаётся извлечь поле [{}]".format(row[0]))
        if len(row) == 3:
            logger.error("Строка [{}] недозаполнена".format(row[0]))
        if len(row) >= 4:
            if (row[2] or row[3]) and not (row[2] and row[3]):
                logger.error("Строка [{}] недозаполнена".format(row[0]))
            if row[2]:
                dct["{}_sh".format(self.en_name)] = row[2]
                dct["split_in_two"] = True
            if row[3]:
                dct["{}_em".format(self.en_name)] = row[3]


class SetOtbor(object):
    def __call__(self, *args):
        type_dict["__type"] = "preliminary_"


parse_dict = {
    "__type": "",
    "Название": GetField("name"),
    "Тип турнира (С - синхрон, М - межрегиональный, Р - региональный)": GetField(
        "type"
    ),
    "Дата начала (первого тура, в том числе отборочного)": GetField(
        "date_start"
    ),
    "Дата окончания (последнего тура, финала)": GetField("date_finish"),
    "Статистика (для турниров с отбором и финалом - данные только по финалу)": None,
    "Статистика (отборочный этап - только для турниров с отдельным отбором)": SetOtbor(),
    "Количество команд": GetQuantity("number_of_teams_manual"),
    "Количество зачетных команд": GetQuantity(
        "number_of_eligible_teams_manual"
    ),
    "Количество регионов": GetQuantity("number_of_regions_manual"),
    "Количество населенных пунктов": GetQuantity("number_of_towns_manual"),
    "Количество туров": GetQuantity("number_of_tours"),
    "Количество вопросов": GetQuantity("number_of_questions"),
    "Общие сведения об отборочном турнире": None,
}


def process_tournament_data(tournament_data):
    tournament = {}
    type_dict["__type"] = ""
    for row in tournament_data:
        try:
            if parse_dict[row[0]]:
                parse_dict[row[0]](row, tournament)
        except KeyError:
            logger.error("Неизвестное поле: {}".format(row[0]))
    eligible_keys = sorted(k for k in tournament if "_eligible" in k)
    for k in eligible_keys:
        k_ = k.replace("_eligible", "")
        if tournament.get(k) and tournament[k] != tournament[k_]:
            tournament[k_] = tournament[k]
    return tournament


team_fields_dict = {
    "№ (Код)": "team_code",
    "Субъект РФ": "region",
    "Код субъекта": "region_code",
    "Населенный пункт": "town",
    "Группа": "category",
    "Название команды": "team_name",
    "Сумма": "questions_total",
    "Перестрелка": "questions_tiebreaker",
    "Место": "standing",
    "Тренер или другое контактное лицо": "contact",
    "email": "email",
    "Телефон": "phone",
}

player_fields_dict = {
    "№ (код) команды": "team_code",
    "Команда": "team_name",
    "Фамилия": "last_name",
    "Имя": "first_name",
    "Отчество": "patronymic",
    "Дата рождения": "date_of_birth",
    "Школа": "school",
    "Класс": "grade",
    "Роль": "role",
    "Город": "town",
}


p_data = json.load(codecs.open("p_data.json", "r", "utf8"))


not_renamed_contacts = set()


def parse_team(team):
    result = {"players": []}
    for k in team:
        try:
            result[team_fields_dict[k]] = (
                process_string(team[k])
                if isinstance(team[k], basestring)
                else team[k]
            )
        except KeyError:
            print(
                "Ошибка: скорее всего на листе «Команды» "
                "есть дополнительные колонки"
            )
            raise
    if str(result.get("team_code", "")).startswith("Строку"):
        return
    if all([not result[x] for x in result]):
        return
    if not result["team_code"]:
        logger.error("Не указан код команды: {}".format(result["team_name"]))
    if not result["team_name"]:
        logger.error(
            "Не указано название команды: {}".format(result["team_code"])
        )
    else:
        result["team_name"] = str(result["team_name"])
    if not result["region_code"]:
        logger.error(
            "Отсутствует код региона (иностранная "
            "команда или ошибка): {}".format(result["team_name"])
        )
    elif not re.search("RU[0-9]{2}", result["region_code"]):
        logger.error("Неверный код региона: {}".format(result["team_name"]))
    if result["category"] not in {"Ш", "М"}:
        logger.error("Неверно указана группа: {}".format(result["team_name"]))
    if not isinstance(result["questions_total"], int):
        logger.error("Неверно указана сумма: {}".format(result["team_name"]))
    pair = (normalize_name(result["team_name"]), result["region_code"])
    if pair in renames:
        result["application"] = 1
        logger.info(
            'Команда "{}" переименована в "{}"'.format(
                result["team_name"], renames[pair]
            )
        )
        result["team_name"] = renames[pair]
        renames_usage[(result["region_code"], renames[pair])] += 1
    else:
        not_renamed_contacts.add(result["email"])
    return result


def full_name(player):
    try:
        return "{} {} {}".format(
            player["first_name"], player["patronymic"], player["last_name"]
        )
    except:
        pass


def process_string(s):
    return s.strip().replace("\n", " ")


def parse_player(player):
    result = {}
    for k in player:
        if k not in player_fields_dict:
            continue
        try:
            result[player_fields_dict[k]] = (
                process_string(player[k])
                if isinstance(player[k], basestring)
                else player[k]
            )
        except:
            pass
    if str(result.get("team_code", "")).startswith("№"):
        return
    if "date_of_birth" not in result:
        print("Скорее всего, неправильно оформлен лист «Игроки»")
        raise
    if result["date_of_birth"]:
        try:
            if isinstance(result["date_of_birth"], basestring):
                result["date_of_birth"] = format(
                    datetime.datetime.strptime(
                        result["date_of_birth"], "%d.%m.%Y"
                    ).date()
                )
            elif isinstance(result["date_of_birth"], datetime.date):
                result["date_of_birth"] = format(result["date_of_birth"])
        except ValueError:
            logger.error(
                "Невозможно распарсить дату рождения игрока: {}".format(
                    full_name(result)
                )
            )
    else:
        logger.error(
            "Не указана дата рождения игрока: {}".format(full_name(result))
        )
    return result


def parse_results_xls(filename):
    global logger
    logger = FileAdapter(main_logger, {"filename": filename})
    logger.info("Обрабатываем файл {}".format(filename))
    tournament = pyexcel.get_array(file_name=filename, sheet_name="Турнир")
    tournament = process_tournament_data(tournament)
    team_data = pyexcel.get_records(
        file_name=filename, sheet_name="Команды", name_columns_by_row=1
    )
    tournament["teams"] = {}
    for team in team_data:
        if not any(x for x in team.values()):
            continue
        parsed_team = parse_team(team)
        if not parsed_team or not parsed_team["region_code"]:
            continue
        parsed_team["tournament_type"] = tournament["type"]
        parsed_team["tournament_name"] = tournament["name"]
        tournament["teams"][parsed_team["team_code"]] = parsed_team
    player_data = pyexcel.get_records(
        file_name=filename, sheet_name="Игроки", name_columns_by_row=1
    )
    for player in player_data:
        if not any(x for x in player.values()):
            continue
        parsed_player = parse_player(player)
        if parsed_player and parsed_player["team_code"] in tournament["teams"]:
            tournament["teams"][parsed_player["team_code"]]["players"].append(
                parsed_player
            )
    tournament["teams"] = list(tournament["teams"].values())
    # split tournament in two by category if needed
    if tournament.get("split_in_two"):
        em = copy.deepcopy(tournament)
        sh = copy.deepcopy(tournament)
        em["teams"] = [x for x in em["teams"] if x["category"] == "М"]
        em.pop("split_in_two")
        em["category"] = "М"
        for k in em.keys():
            if k.endswith("_em"):
                kk = k[: -len("_em")]
                logger.info("setting {} to {}: {}".format(kk, k, em[k]))
                em[kk] = em[k]
        sh["teams"] = [x for x in sh["teams"] if x["category"] == "Ш"]
        sh["category"] = "Ш"
        for k in sh.keys():
            if k.endswith("_sh"):
                kk = k[: -len("_sh")]
                logger.info("setting {} to {}: {}".format(kk, k, sh[k]))
                sh[kk] = sh[k]
        sh.pop("split_in_two")
        tournaments = [em, sh]
    else:
        tournament["category"] = "mixed"
        tournaments = [tournament]
    # count tournament level and team scores
    for tournament in tournaments:
        shchr_bonus = 0
        tournament["shchr_boni"] = []
        for team in tournament["teams"]:
            for player in team["players"]:
                candidates = [
                    x
                    for x in p_data
                    if full_name(x) == full_name(player)
                    and x.get("date_of_birth")
                    and x.get("date_of_birth") == player["date_of_birth"]
                ]
                if not candidates:
                    candidates = [
                        x for x in p_data if full_name(x) == full_name(player)
                    ]
                if candidates:
                    place = candidates[0]["place"]
                    candidate_bonus = 0.2 / math.sqrt(place)
                    tournament["shchr_boni"].append(
                        {
                            "name": full_name(player),
                            "place": place,
                            "bonus": candidate_bonus,
                        }
                    )
                    shchr_bonus += candidate_bonus
        if tournament["type"] == "С":
            shchr_bonus *= 0.1
        tournament["number_of_teams"] = len(tournament["teams"])
        tournament["number_of_regions"] = len(
            {x["region_code"] for x in tournament["teams"] if x["region_code"]}
        )
        tournament["number_of_towns"] = len(
            {x["town"] for x in tournament["teams"] if x["town"]}
        )
        for p in ["number_of_teams", "number_of_regions", "number_of_towns"]:
            pman = "{}_manual".format(p)
            if tournament[p] != tournament[pman]:
                logger.error(
                    "Показатель {} не совпадает: ручной подсчёт - {}, "
                    "автоматический - {}".format(
                        p, tournament[pman], tournament[p]
                    )
                )
        teams = (
            tournament["number_of_teams"]
            + (tournament.get("preliminary_number_of_teams") or 0) * 0.1
        )
        if tournament["type"] == "С":
            teams *= 0.1
        level_T = min(teams, 50)

        regions = sorted({x["region_code"] for x in tournament["teams"]})
        if tournament["type"] == "Р" and regions[0] in {"RU77", "RU78"}:
            tournament["number_of_towns"] = 5
        level_R = {
            "Р": min(tournament["number_of_towns"], 5),
            "С": tournament["number_of_regions"] * 0.3,
            "М": min(tournament["number_of_regions"], 10),
        }[tournament["type"]]
        level_B = (1 + shchr_bonus) * 100
        tournament["T"] = level_T
        tournament["R"] = level_R
        tournament["B"] = level_B
        try:
            tournament["level"] = (
                math.log(level_T) * math.sqrt(level_R) * level_B
            )
        except ValueError:
            pass
        tournament["teams"] = sorted(
            tournament["teams"],
            key=lambda x: x["questions_total"],
            reverse=True,
        )
        write_standings(tournament["teams"], type_=tournament["type"])
        score_teams(tournament)
    return tournaments


def score_teams(tournament):
    total_teams = len(tournament["teams"])
    m_teams = len([x for x in tournament["teams"] if x["category"] == "М"])
    for team in tournament["teams"]:
        if "вне" in str(team["standing"]):
            team["score"] = 0
            continue
        team["score"] = tournament["level"] * (
            1 / mean(team["standing_for_scoring"]) - 1 / total_teams
        )
        if tournament["category"] == "mixed" and team["category"] == "М":
            team["score"] *= 1 + (
                (total_teams - m_teams)
                / (
                    total_teams
                    * math.sqrt(mean(team["standing_for_scoring_in_category"]))
                )
            )
        team["score"] = round(team["score"])


def mean(list_):
    return sum(list_) / len(list_)


def tabulate(*args):
    return "\t".join(map(format, args))


class WorksheetWriter(object):
    def __init__(self, worksheet, initrow=2):
        self.ws = worksheet
        self.row = initrow

    def __call__(self, lst):
        col = 1
        for el in lst:
            self.ws.cell(row=self.row, column=col).value = el
            col += 1
        self.row += 1


class GoogleSheetWriter(object):
    def __init__(self, worksheet, initrow=1):
        self.ws = worksheet
        self.row = initrow
        self.cells = []

    def __call__(self, lst, sep="\t"):
        if isinstance(lst, basestring):
            lst = lst.split(sep)
        col = 1
        for el in lst:
            cell = Cell(self.row, col, el)
            self.cells.append(cell)
            col += 1
        self.row += 1

    def update(self):
        self.ws.update_cells(self.cells)
        self.cells = []


def t_for_dump(tournament):
    new = copy.deepcopy(tournament)
    new.pop("teams")
    return new


class DebugWriter(object):
    def __init__(self, fn):
        self.fn = fn
        self.buffer = ""
        f = codecs.open(self.fn, "w", "utf8")
        f.close()

    def __call__(self, lst, sep="\t"):
        if isinstance(lst, basestring):
            lst = lst.split(sep)
        self.buffer += "\t".join(map(format, lst)) + "\n"

    def update(self):
        f = codecs.open(self.fn, "a", "utf8")
        f.write(self.buffer)
        f.close()
        self.buffer = ""


good_contacts = set()
team_to_email = {}


def parse_application(filename):
    global good_contacts
    logger = FileAdapter(main_logger, {"filename": filename})
    logger.info("Обрабатываем файл {}".format(filename))
    team_sheet = pyexcel.get_records(
        file_name=filename, sheet_name="Команда", name_columns_by_row=1
    )
    tournaments_sheet = pyexcel.get_records(
        file_name=filename, sheet_name="Турниры", name_columns_by_row=1
    )
    contacts_sheet = pyexcel.get_records(
        file_name=filename, sheet_name="Контакты", name_columns_by_row=0
    )
    emails = {
        rec["Email"]
        for rec in contacts_sheet
        if isinstance(rec["Email"], basestring)
    }
    emails.discard("")
    good_contacts |= emails
    try:
        team_base_name = str(team_sheet[1]["Команда"])
    except:
        logger.info("error on {}".format(filename))
        return
    normalized = normalize_name(team_base_name)
    names[normalized] = team_base_name
    team_base_region = team_sheet[1]["Код региона"].strip().upper()
    for tournament in tournaments_sheet[1:]:
        renames[
            (normalize_name(tournament["Команда"]), team_base_region)
        ] = team_base_name
    renames_usage[(team_base_region, team_base_name)] = 0
    team_to_email[(team_base_region, normalized)] = emails


def rate_sort(rc):
    return sorted(rc, key=lambda x: rc[x]["score"], reverse=True)


def process_invite(invite, common_config):
    if not invite:
        return invite
    if isinstance(invite, str):
        invite = datetime.datetime.strptime(invite, "%Y-%m-%d").date()
    threshold = common_config["preliminary_threshold"]
    return str(invite) if invite < threshold else "{} (?)".format(invite)


def collect_players(results):
    rosters = {}
    for result in results["results"]:
        if result["score"] == 0:
            continue
        rosters[result["tournament_name"]] = {
            full_name(x) for x in result["players"]
        }
    return rosters


def format_players(rosters):
    result = ""
    for t in sorted(rosters):
        result += "В турнире {} за команду сыграли: {}\n\n".format(
            t, ", ".join(sorted(rosters[t]))
        )
    return result


def count_ratings_with_breakpoint(
    tournaments,
    by_team,
    ratings,
    signature,
    inv_date,
    f,
    prop_invites,
    pi_file,
    pi_file_hr,
    rejects,
):
    logger = FileAdapter(
        main_logger, {"filename": "Приглашения {}".format(inv_date)}
    )
    for tournament in tournaments:
        logger.debug(
            "Обрабатываем турнир {} категории {}".format(
                tournament["name"], tournament["category"]
            )
        )
        f.write(
            tabulate(
                tournament["name"],
                tournament["type"],
                tournament["category"],
                tournament["R"],
                tournament["T"],
                tournament["B"],
                tournament["level"],
            )
            + "\n"
        )
        for team in tournament["teams"]:
            by_team[
                (
                    normalize_name(team["team_name"]),
                    team["category"],
                    team["region_code"],
                )
            ].append(team)
            normalized = normalize_name(team["team_name"])
            if normalized not in names:
                names[normalized] = team["team_name"]
    pyaml.dump(tournaments, codecs.open("tournaments.yaml", "w", "utf8"))
    pyaml.dump(
        [t_for_dump(x) for x in tournaments],
        codecs.open("tournaments_short.yaml", "w", "utf8"),
    )
    for team in by_team:
        results = by_team[team]
        application = any([x.get("application", 0) for x in results])
        name = team[0]
        category = team[1]
        town = team[2]
        srt = sorted(results, key=lambda x: x["score"], reverse=True)
        och = sorted(
            [x for x in results if x["tournament_type"] != "С"],
            key=lambda x: x["score"],
            reverse=True,
        )
        och_score = 0
        if och:
            och_score = och[0]["score"]
        srtx = srt
        if och:
            srtx = [
                x
                for x in srtx
                if x["tournament_name"] != och[0]["tournament_name"]
            ]
        other_score = 0
        if srtx:
            other_score = srtx[0]["score"]
        ratings[category][(name, town, application)] = {
            "score": och_score + other_score,
            "results": srt,
        }
    logger.info("Вычисляем приглашения")
    for category in sorted(ratings):
        pi_file_hr.append("Категория {}:\n".format(category))
        source_regions = len(prop_invites[category]["regions"])
        source_teams = len(prop_invites[category]["teams"])
        target_regions = source_regions + signature[category]["regions"]
        target_teams = source_teams + signature[category]["teams"]
        for team_tup in rate_sort(ratings[category]):
            if len(prop_invites[category]["regions"]) == target_regions:
                break
            # if not team_tup[2]:
            #     continue
            team_tup_inv = (team_tup[1], team_tup[0])
            if team_tup_inv in prop_invites[category]["teams"]:
                continue
            if team_tup[1] not in (prop_invites[category]["regions"]):
                prop_invites[category]["teams"][team_tup_inv] = inv_date
                prop_invites[category]["regions"].add(team_tup[1])
                if (
                    team_tup_inv in rejects
                    and rejects[team_tup_inv]["date"] <= inv_date
                ):
                    target_regions += 1
                    target_teams += 1
                    logger.info(
                        "Количество приглашений увеличено "
                        "из-за отказа команды {}".format(names[team_tup[0]])
                    )
                pi_file.append(
                    tabulate(
                        category, inv_date, team_tup[1], names[team_tup[0]]
                    )
                )
                pi_file_hr.append(
                    "{} ({})".format(
                        names[team_tup[0]], code_to_region[team_tup[1]]
                    )
                )
                logger.info(
                    "Команда {r} {} получает приглашение в категории {} "
                    "как представитель региона {r}".format(
                        names[team_tup[0]], category, r=team_tup[1]
                    )
                )
                players = collect_players(ratings[category][team_tup])
                logger.info(format_players(players))
        pcr = prop_invites[category]["regions"]
        scr = signature[category]["regions"]
        logger.info(
            "Из {} региональных приглашений категории {} выдано {}".format(
                scr, category, len(pcr) - source_regions
            )
        )
        if (len(pcr) - source_regions) < scr:
            logger.info(
                "{} дополнительных приглашений уходит в лист ожидания".format(
                    scr - (len(pcr) - source_regions)
                )
            )
        for team_tup in rate_sort(ratings[category]):
            if len(prop_invites[category]["teams"]) == target_teams:
                break
            # if not team_tup[2]:
            #     continue
            team_tup_inv = (team_tup[1], team_tup[0])
            if team_tup_inv in prop_invites[category]["teams"]:
                continue
            else:
                prop_invites[category]["teams"][team_tup_inv] = inv_date
                if team_tup in rejects and rejects[team_tup_inv] < inv_date:
                    target_teams += 1
                    logger.info(
                        "Количество приглашений увеличено "
                        "из-за отказа команды {}".format(names[team_tup[0]])
                    )
                pi_file.append(
                    tabulate(
                        category, inv_date, team_tup[1], names[team_tup[0]]
                    )
                )
                pi_file_hr.append(
                    "{} ({})".format(
                        names[team_tup[0]], code_to_region[team_tup[1]]
                    )
                )
                logger.info(
                    "Команда {r} {} получает приглашение в категории {} "
                    "как обладатель лучшего балла из листа ожидания".format(
                        names[team_tup[0]], category, r=team_tup[1]
                    )
                )
        pcr = prop_invites[category]["teams"]
        scr = signature[category]["teams"]
        logger.info(
            "Из {} приглашений категории {} выдано {}".format(
                scr, category, len(pcr) - source_teams
            )
        )
        pi_file_hr.append("")


class WriterWrapper(object):
    def __init__(self, *args, **kwargs):
        self.writers = args

    def __call__(self, *args, **kwargs):
        for w in self.writers:
            w(*args, **kwargs)


def process_rejects(rej):
    return {(x["region"], normalize_name(x["name"])): x for x in rej}


def format_invite(invite, reject):
    if not invite:
        return ""
    if invite and reject:
        return "{} (отказ)".format(invite.replace(" (?)", ""))
    return invite


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--invite_config", default="invite_config.yaml")
    parser.add_argument("--debug", action="store_true")
    args = parser.parse_args()

    with codecs.open(args.invite_config, "r", "utf8") as f:
        invite_config = pyaml.yaml.safe_load(f)
    if "rejects" in invite_config["common"]:
        rejects = process_rejects(invite_config["common"]["rejects"])
    else:
        rejects = set()
    global logger
    wd = os.path.abspath(os.getcwd())
    region_to_code = json.load(codecs.open("region_to_code.json", "r", "utf8"))
    for x in region_to_code:
        code_to_region["RU{}".format(region_to_code[x])] = x
    os.chdir("zayavki")
    for filename in os.listdir(os.getcwd()):
        if filename.endswith((".xls", ".xlsx")) and not filename.startswith(
            (".", "~")
        ):
            parse_application(filename)
    os.chdir(wd)
    os.chdir("vyvod2019")
    tournaments = []
    for filename in os.listdir(os.getcwd()):
        if filename.endswith((".xls", ".xlsx")) and not filename.startswith(
            (".", "~")
        ):
            tournaments.extend(parse_results_xls(filename))
    logger = FileAdapter(main_logger, {"filename": "main"})
    logger.info("Обработка отчётов закончена")
    bad_renames = [x for x in renames_usage if not renames_usage[x]]
    for x in sorted(bad_renames):
        logger.warning(
            "Не удалось найти заявившуюся команду "
            "в результатах турниров: {} {}".format(*x)
        )
    os.chdir(wd)
    logger.info("Обрабатываем приглашения")
    invites = defaultdict(lambda: {"teams": {}, "regions": set()})
    with codecs.open("invites.tsv", "r", "utf8") as f:
        for line in f:
            tabs = line.strip().split("\t")
            if len(tabs) < 4:
                continue
            invites[tabs[0]]["teams"][
                (tabs[2], normalize_name(tabs[3]))
            ] = tabs[1]
            if len(tabs) == 4 or (len(tabs) == 5 and tabs[4].lower() != "нет"):
                invites[tabs[0]]["regions"].add(tabs[2])
    for cat in sorted(invites):
        assert isinstance(invites[cat]["regions"], set)
    logger.info("Создаём вспомогательные файлы")
    with codecs.open("tournament_names.txt", "w", "utf8") as f:
        f.write(
            "\n".join(
                sorted({tournament["name"] for tournament in tournaments})
            )
        )
    with codecs.open("not_renamed_emails.txt", "w", "utf8") as f:
        f.write("\n".join(sorted((not_renamed_contacts - good_contacts))))
    with codecs.open("good_emails.txt", "w", "utf8") as f:
        f.write("\n".join(sorted(good_contacts)))
    logger.info("Строим рейтинги")
    by_team = defaultdict(list)
    ratings = defaultdict(lambda: defaultdict(dict))
    # wb = openpyxl.load_workbook('sorted_ratings_template.xlsx')
    f = codecs.open("tournaments.tsv", "w", "utf8")
    f.write(
        tabulate("Турнир", "Тип", "Категория", "R", "T", "Бонус", "Уровень")
        + "\n"
    )
    low_threshold = datetime.date(2000, 1, 1)
    prop_invites = defaultdict(dict)
    for category in ["М", "Ш"]:
        prop_invites[category]["regions"] = (
            invites[category]["regions"] or set()
        )
        prop_invites[category]["teams"] = invites[category]["teams"]
    pi_file = []
    pi_file_hr = []
    common_config = invite_config.pop("common")
    for date in sorted(invite_config):
        tournaments_ = [
            x
            for x in tournaments
            if x["date_finish"] > low_threshold and x["date_finish"] <= date
        ]
        count_ratings_with_breakpoint(
            tournaments_,
            by_team,
            ratings,
            invite_config[date],
            date,
            f,
            prop_invites,
            pi_file,
            pi_file_hr,
            rejects,
        )
        low_threshold = date
    f.close()
    with codecs.open("prop_invites.tsv", "w", "utf8") as f:
        f.write("\n".join(pi_file) + "\n")
    with codecs.open("prop_invites_hr.tsv", "w", "utf8") as f:
        f.write("\n".join(pi_file_hr) + "\n")
    for category in prop_invites:
        invites[category]["teams"].update(prop_invites[category]["teams"])
    logger = FileAdapter(main_logger, {"filename": "main"})
    inv = codecs.open("invites_emails.tsv", "w", "utf8")
    all_emails = codecs.open("all_emails.tsv", "w", "utf8")
    if not args.debug:
        table = gc.open_by_key("1DshUJVaOk23PXpOjffP20sRLs5tFajbEx6G_wdkg0hk")
        sheets = table.worksheets()
        sheets_dict = {s.title: s for s in sheets}
    for cat in sorted(ratings):
        rc = ratings[cat]
        srt = rate_sort(rc)
        filename = "ratings_{}.tsv".format(cat)
        writers = [DebugWriter(filename)]
        if not args.debug:
            writers.append(GoogleSheetWriter(sheets_dict[cat]))
            sheets_dict[cat].clear()
        write = WriterWrapper(*writers)
        write(
            "Приглашение\tЗаявка\tНазвание\tРегион\tИтоговые очки\t"
            + "\t".join(
                [
                    "Турнир{i}\tОчки{i}\tМесто{i}".format(i=i)
                    for i in range(1, 7)
                ]
            )
            + "\n"
        )
        for team in srt:
            results = []
            pair = (team[1], team[0])
            for r in rc[team]["results"]:
                results.extend(
                    [
                        r["tournament_name"],
                        int(r["score"]),
                        mean(r["standing_for_scoring"]),
                    ]
                )
            reject = pair in rejects
            invite = process_invite(
                invites[cat]["teams"].get(pair, ""), common_config
            )
            write(
                tabulate(
                    *(
                        [
                            format_invite(invite, reject),
                            "да" if team[2] else "",
                            names[team[0]],
                            code_to_region[team[1]],
                            int(rc[team]["score"]),
                        ]
                        + results
                    )
                )
            )
            if invites[cat]["teams"].get(pair, "") and not reject:
                inv.write(
                    tabulate(
                        cat,
                        team[1],
                        names[team[0]],
                        ",".join(
                            sorted(team_to_email.get(pair, ["нетзаявки"]))
                        )
                        + "\n",
                    )
                )
            if team[2]:
                all_emails.write(
                    tabulate(
                        cat,
                        team[1],
                        names[team[0]],
                        ",".join(
                            sorted(team_to_email.get(pair, ["нетзаявки"]))
                        ),
                    )
                    + "\n"
                )
        for team in bad_renames:
            write(
                tabulate(
                    *(
                        [
                            invites[cat]["teams"].get((team[0], team[1]), ""),
                            "да",
                            team[1],
                            code_to_region[team[0]],
                            0,
                        ]
                    )
                )
            )
        for writer in write.writers:
            writer.update()
    inv.close()
    all_emails.close()


if __name__ == "__main__":
    main()
